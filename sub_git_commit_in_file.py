#!/usr/bin/python
#
# Author: 	Joe Alam
# Created: 	September 2014
#
# This is a simple script that can be used to insert the SHA1 hash of the current Git commit into a file
# based on a specified substitution. For example, substitute it into a C++ header file.
#
# Sample call: python sub_git_commit_in_file.py -f version.txt -s '(?<=gitVersion=")(?:\\.|[^"\\])*(?=")'
# For Windows, unless the git install directories have been added to your system path, you'll need to specify
# the full path of the git exe with the -e (or --gitpath) parameter
# For example: C:\Python\python.exe sub_git_commit_in_file.py -f version.txt -e "C:\Git\cmd\git.exe" -s '(?<=gitVersion=")(?:\\.|[^"\\])*(?=")'

import sys, getopt, re, subprocess

# doSubstitution
#
# This function does the body of the work, retrieving the git commit hash, performing the substitution, and writing the output to the file
def doSubstitution(filename, subFormat, gitPath):
	# Get the git commit
	version = subprocess.check_output([gitPath, "rev-parse", "-q", "HEAD"]).strip()
	print("Version: ", version)
	
	# Here's a regex string that will write the version within gitVersion="[HERE]" in a file
	# Example awesome regex: (?<=gitVersion=")(?:\\.|[^"\\])*(?=")
	# Here's another regex string that will write the version within GITVERSION "[HERE]" in a file
	# Example awesome regex: (?<=GITVERSION ")(?:\\.|[^"\\])*(?=")
	print("Substitution format (regex): ", subFormat)
	inFile = open(filename, "rt")	
	content = inFile.read()	
	inFile.close()
	print("Opened ", filename)
	
	if re.search(r"" + subFormat, content) == None:
		print(filename, " does not contain specified substitution string")
		sys.exit(2)
		
	# Open the file for writing and write the substituted version
	outFile = open(filename, "wt")
	outFile.write(re.sub(r"" + subFormat, version.decode(), content))
	outFile.close()
	
	print("Substitution performed successfully")

# main
#
# The main function simply parses the command line arguments and calls doSubstitution
def main(argv):
	print("----------------------------------------------------------")
	print("------------ Running sub_git_commit_in_file.py -----------")
	filename = ""
	subFormat = "gitVersion=\"*\""
	gitPath = "git"

	try:
		opts, args = getopt.getopt(argv,"hf:s:e:", ["file=", "format=", "gitpath="])
	except getopt.GetoptError:
		print("sub_git_commit_in_file.py -f <filename> -s <subtitution format>")
		sys.exit(2)
	for opt, arg in opts:
		if opt == "-h":
			print("sub_git_commit_in_file.py -f <filename> -s <subtitution format>")
			sys.exit()
		elif opt in ("-f", "--file"):
			filename = arg
		elif opt in ("-s", "--format"):
			subFormat = arg
		elif opt in ("-e", "--gitpath"):
			gitPath = arg
	
	doSubstitution(filename, subFormat, gitPath)
	
	print("----------------------------------------------------------")

# Call the main function with the command line arguments
if __name__ == "__main__":
	main(sys.argv[1:])
