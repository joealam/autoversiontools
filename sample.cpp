#include <iostream>
#include "Config.h"

int main(int argc, char* argv[])
{
	std::cout << "Version: " << VERSION_MAJOR << "." << VERSION_MINOR << "." << GITVERSION << std::endl;

	system("pause");
	return 0;
}
